//
//  EditTaskViewController.swift
//  DayPlanner
//
//  Created by Adrian Watzlawczyk on 27/05/15.
//  Copyright (c) 2015 Adrian Watzlawczyk. All rights reserved.
//

import UIKit

class EditTaskViewController: UIViewController {
    
    var newTask: Bool = true
    var taskItem: AnyObject? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
