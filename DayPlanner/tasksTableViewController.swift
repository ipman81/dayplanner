//
//  tasksTableViewController.swift
//  DayPlanner
//
//  Created by Adrian Watzlawczyk on 27/05/15.
//  Copyright (c) 2015 Adrian Watzlawczyk. All rights reserved.
//

import UIKit
import CoreData

class tasksTableViewController: UITableViewController, NSFetchedResultsControllerDelegate {

    var managedObejctContext:NSManagedObjectContext? = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }

    @IBAction func backPressed(sender: AnyObject) {
        self.navigationController?.popToRootViewControllerAnimated(true)
    }
    
    // Mark: - Segues
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        let controller = segue.destinationViewController as! EditTaskViewController
        
        if segue.identifier == "newTask"{
            
            controller.newTask = true
        
        }else if segue.identifier == "addedTask"{
            
            
        
        }
    }
    
    //Mark: - Fetched Results Controller
    
    var fetchedResultsController: NSFetchedResultsController {
        if _fetchResultsController != nil {
            
            return _fetchResultsController!
            
        }
        
        let fetchRequest = NSFetchRequest()
        let entity = NSEntityDescription.entityForName("Tasks", inManagedObjectContext: managedObejctContext!)
        fetchRequest.entity = entity
        fetchRequest.fetchBatchSize = 20
        
        let sortDescriptor = NSSortDescriptor(key: "taskDateStamp", ascending: false)
       
        
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        let aFetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: self.managedObejctContext!, sectionNameKeyPath: "taskDate", cacheName: nil)
        aFetchResultsController.delegate = self
        _fetchResultsController = aFetchResultsController

        var error: NSError? = nil
        
        if !_fetchResultsController!.performFetch(&error){
            abort()
        }
        
        return _fetchResultsController!
    }
    
    var _fetchResultsController:NSFetchedResultsController? = nil
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        self.tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        switch type {
        case .Insert:
            self.tableView.insertSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        case .Delete:
            self.tableView.deleteSections(NSIndexSet(index: sectionIndex), withRowAnimation: .Fade)
        default:
            return
        }
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch type {
        case .Insert:
            self.tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
             self.tableView.deleteRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Update:
            self.configureCell(tableView.cellForRowAtIndexPath(indexPath!)!, atIndexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        
        default:
            return
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        self.tableView.endUpdates()
    }
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
       
        var nuOfSection: Int? = self.fetchedResultsController.sections?.count
        
        if nuOfSection == 0 {
            nuOfSection = 1
        }
        
        return nuOfSection!
    
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.fetchedResultsController.sections!.count > 0 {

            let sectionInfo = self.fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
            return sectionInfo.numberOfObjects
        }else{
            return 0
        }

    }

    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        if self.fetchedResultsController.sections!.count > 0 {
            let sectionInfo = self.fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
            return sectionInfo.name!
        }else{
                return ""
        }
        
        
    }
    
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController.managedObjectContext
            
            context.deleteObject(self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject)
            
            var error: NSError? = nil
            
            if context.save(&error){
                abort()
            }
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! UITableViewCell
        self.configureCell(cell, atIndexPath: indexPath)
        return cell
    }
    
    
    func configureCell(cell: UITableViewCell, atIndexPath indexPath:NSIndexPath){
        let object = self.fetchedResultsController.objectAtIndexPath(indexPath) as! NSManagedObject
        cell.textLabel?.text = object.valueForKey("taskDescription")!.description
        
        let taskStatus = object.valueForKey("tastStatus")!.description
        let taskDate = object.valueForKey("taskDate")!.description
        
        cell.detailTextLabel?.font = UIFont(name: "Avenir-Medium", size: 12.0)
        cell.detailTextLabel?.text = "\(taskStatus) (\(taskDate))"
    }
    
    
    
    
    
    
    
}
